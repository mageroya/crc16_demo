# CRC16 Checksum Implementation and Demo 
#
# Copyright (c) 2014 Roman Bendt
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
# of the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
# PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

EXECUTABLE = crc.x
PACKFILE = crc_src
SIGNING_IDENTITY = E128B5AE

LIBOBJS = 

SRCS := $(wildcard src/*.cpp)
OBJS := $(addprefix obj/,$(notdir $(SRCS:.cpp=.o)))
HEADS := $(wildcard src/*.hpp)

CC = g++ -std=c++11 -m64
DEBUG = -g3 -O0
AFLAGS = -Wall -Wextra -Wconversion -Wsign-compare -Wshadow -Wzero-as-null-pointer-constant -pedantic $(DEBUG)
CFLAGS = -c 
LDFLAGS = -pthread

all: dirs bin/$(EXECUTABLE)

bin/$(EXECUTABLE): $(OBJS)
	@echo "[\033[94mlinking\033[0m] "$@
	@$(CC) $(AFLAGS) -o $@ $^ $(LIBOBJS) $(LDFLAGS) 
	

obj/%.o: src/%.cpp $(HEADS)
	@echo "[\033[94mcompiling\033[0m] "$<"..."
	@$(CC) $(AFLAGS) $(CFLAGS) -o $@ $<

.PHONY: tidy clean re pack verify dirs init

init:
	@$(MAKE) --no-print-directory dirs
	@printf "#include <iostream>\n\nint main(int argc, const char* argv[])\n{\n\tfor (int i = 0; i < argc; i++)\n\t{\n\t\tstd::cout << \"ARG\" << i << \": \" << argv[i] << std::endl;\n\t}\n\treturn 0;\n}" >> src/main.cpp
	@git init

dirs:
	@mkdir -p bin obj src
	@ln -fs bin/$(EXECUTABLE) runnable

re:
	@$(MAKE) --no-print-directory clean
	@$(MAKE) --no-print-directory all
	@$(MAKE) --no-print-directory pack
	@$(MAKE) --no-print-directory verify

tidy: 
	@echo "[\033[94mtidying project\033[0m]"
	@rm -rf obj
	@rm -f core

clean: tidy
	@echo "[\033[94mcleaning project\033[0m]"
	@rm -rf bin 
	@rm -f *.gz *.sig runnable

pack:
	@echo "[\033[94mpacking source\033[0m]"
	@tar cvf - src makefile | pigz -p4 -9 > $(PACKFILE).tar.gz
	@echo "[\033[94msigning packfile\033[0m]"
	@gpg -u $(SIGNING_IDENTITY) --output $(PACKFILE).tar.gz.sig --detach-sig $(PACKFILE).tar.gz
	@tar cvf - *.tar.gz* | pigz -p4 -9 > $(PACKFILE).signed.tar.gz

verify:
	@echo "[\033[94mverifying signature\033[0m]"
	@gpg --verify $(PACKFILE).tar.gz.sig $(PACKFILE).tar.gz

