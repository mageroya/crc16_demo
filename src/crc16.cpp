/*
 * CRC16 Checksum Implementation and Demo 
 *
 * Copyright (c) 2014 Roman Bendt
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "crc16.hpp"

/*
 * helper function for calculating crc checksum. digests the next byte to include in the crc. 
 */
uint16_t _crc16_update(uint16_t crc, uint8_t a)
{
	uint8_t i = 0;
	crc = (uint16_t)(crc ^ a);
	for (i = 0; i < 8; ++i)
	{
		if (crc & 1) crc = (uint16_t)((crc >> 1) ^ 0xA001);
		else crc = (uint16_t)(crc >> 1);
	}
	return crc;
}

uint16_t calculate_checksum_crc16(std::vector<uint8_t> data)
{
	uint16_t _crc = 0xFFFF;
	for (uint8_t i : data)
	{
		_crc = _crc16_update(_crc, i);
	}
	return _crc;
}

bool verify_checksum_crc16(std::vector<uint8_t> data, uint16_t checksum)
{
	uint16_t _crc = 0xFFFF;
	for (uint8_t i : data)
	{
		_crc = _crc16_update(_crc, i);
	}
	_crc = _crc16_update(_crc, (uint8_t)(checksum & 0xFF));
	_crc = _crc16_update(_crc, (uint8_t)((checksum>>8) & 0xFF));
	
	return (_crc == 0);
}

