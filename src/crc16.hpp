/*
 * CRC16 Checksum Implementation and Demo 
 *
 * Copyright (c) 2014 Roman Bendt
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/*
 * implements a CRC16 checksum using 
 * base polynom: 0xA001
 * initial value: 0xFFFF
 */

#ifndef CRC16_H
#define CRC16_H

#include <cstdint>
#include <vector>

/*
 * calculates a crc16 from the given, arbitrary-lenght data.
 * returns crc16 checksum to transmit with the data.
 */
uint16_t calculate_checksum_crc16(std::vector<uint8_t> data);

/*
 * verifys a given crc16 checksum against the given data.
 * returns true if data is error free, false if data is corrupted.
 */
bool verify_checksum_crc16(std::vector<uint8_t> data, uint16_t checksum);

#endif

