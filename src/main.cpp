/*
 * CRC16 Checksum Implementation and Demo 
 *
 * Copyright (c) 2014 Roman Bendt
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
 * of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 * INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 * HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <iostream>
#include <vector>
#include "crc16.hpp"

// define to disable assertions
//#define NDEBUG
#include <cassert>

int main(int argc, const char* argv[])
{
	std::cout << "CRC16 Checksum demo program" << std::endl;

	for (int i = 0; i < argc; i++)
	{
		std::cout << "ARG" << i << ": " << argv[i] << std::endl;
	}
	
	std::vector<uint8_t> my_data;

	my_data.push_back(0x00);
	my_data.push_back(0x10);
	my_data.push_back(0x20);
	my_data.push_back(0x25);
	my_data.push_back(0x30);
	my_data.push_back(0x35);
	my_data.push_back(0x40);
	my_data.push_back(0x45);
	
	uint16_t my_crc = calculate_checksum_crc16(my_data);
	std::cout << "CRC is 0x" << std::hex << my_crc << std::endl;	

	// check correct checksum on unmodified data
	assert(verify_checksum_crc16(my_data, my_crc));
	
	// check failing checksum on modified leading zeros
	my_data.at(0) = 0x01;
	assert(!verify_checksum_crc16(my_data, my_crc));
	my_data.at(0) = 0x00;

	// check failing checksum on modified body data
	my_data.at(3) = 0x24;
	assert(!verify_checksum_crc16(my_data, my_crc));
	my_data.at(3) = 0x25;

	// check failing checksum on missing data
	my_data.pop_back();
	assert(!verify_checksum_crc16(my_data, my_crc));
	
	// check correct checksum on repaired data
	my_data.push_back(0x45);
	assert(verify_checksum_crc16(my_data, my_crc));

	// check failing checksum on appended data
	my_data.push_back(0x50);
	assert(!verify_checksum_crc16(my_data, my_crc));

	// check correct checksum on repaired data
	my_data.pop_back();
	assert(verify_checksum_crc16(my_data, my_crc));

	// check failing checksum on appended zeros
	my_data.push_back(0x00);
	assert(!verify_checksum_crc16(my_data, my_crc));
	
	// check correct checksum on repaired data
	my_data.pop_back();
	assert(verify_checksum_crc16(my_data, my_crc));

	// check failing checksum on prepended zeros
	my_data.insert(my_data.begin(), 0x00);
	assert(!verify_checksum_crc16(my_data, my_crc));

	std::cout << "ALL CHECKS PASSED" << std::endl;

	return 0;
}

